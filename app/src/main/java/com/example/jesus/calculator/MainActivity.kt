package com.example.jesus.calculator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import net.objecthunter.exp4j.ExpressionBuilder


class MainActivity : AppCompatActivity() {
    private val tokens = arrayOf("+", "-", ".", "*", "/")
    private val etComputationLazy = lazy { findViewById<EditText>(R.id.editText) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val et = etComputationLazy.value
        et.showSoftInputOnFocus = false

    }

    fun OnClickNumberOrSign(v: View) {
        val et = etComputationLazy.value
        val btn = if (v.id == R.id.btnBorrarUnCaracter) v as ImageButton else v as Button
        val textBtn = if (btn is Button) (btn as Button).text else "ButtonImage"
        val textEt = et.text

        when {
            textBtn in ((0..10).map { x -> x.toString() }) || textBtn in tokens -> {
                et.setText("$textEt$textBtn")
            }
            'C' in textBtn -> et.text.clear()
            "+/-" in textBtn -> et.setText("$textEt-")
            "( )" in textBtn ->
                if ('(' in textEt) et.setText("$textEt)")
                else et.setText("$textEt(")
            textBtn == "ButtonImage" -> {
                val pos = et.selectionStart
                if (pos == 0) Unit else {
                    et.text = et.text.delete(pos - 1, pos)
                    et.setSelection(et.length())
                }

            }
        }
    }

    fun onClickComputeResult(v: View) {
        val et = etComputationLazy.value
        val strExpresion = et.text.toString()

        val sanitaze = try {
            ExpressionBuilder(strExpresion).build().evaluate().toString()
        } catch (e: Exception) {
            "Error"
        }
        et.setText(sanitaze)
    }
}

